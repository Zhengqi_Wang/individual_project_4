# Demo Video
[![Watch the video](https://img.youtube.com/vi/X5TjQK4WZK8/0.jpg)](https://www.youtube.com/watch?v=X5TjQK4WZK8 "Watch the video")



# Rust AWS Lambda Function

## Overview

This project contains a Rust-based AWS Lambda function designed to calculate the square root of a given input number. Utilizing the power of Rust, this function provides high performance and safety for serverless computing on AWS.

## Prerequisites

Before you deploy and run this Lambda function, you need the following:
- Rust programming environment.
- AWS CLI configured with appropriate AWS account credentials.
- `cargo-lambda`, a Cargo subcommand that simplifies building, deploying, and managing AWS Lambda functions in Rust.

## Functionality

The function accepts a JSON object containing a single key `value` that maps to the number for which the square root is to be calculated. It returns the square root of this number in its response.

## Development

The function is written in Rust and uses the following key dependencies:
- `lambda_runtime` for handling the Lambda's runtime specifics.
- `serde` and `serde_json` for parsing and outputting JSON.

### File Structure

- `src/main.rs`: Contains the Lambda function's code.
- `Cargo.toml`: Manages dependencies and project settings.

## Deployment

To deploy this Lambda function to AWS Lambda, follow these steps:

1. **Build the Lambda Function**: 
   Compile the function for the AWS environment, targeting the required architecture (e.g., ARM64 for AWS Graviton processors).
   ```bash
   cargo lambda build --release --arm64
   ```

2. **Deploy to AWS Lambda**:
    Deploy your function to the AWS cloud using cargo-lambda.
    ```bash
    cargo lambda deploy --region us-east-2
    ```

3. **Invoke the Function**:
    You can test the Lambda function by invoking it with a sample payload:
    ```bash
    cargo lambda invoke --data-ascii '{"value": 100}'
    ```

## Local Development and Testing

For local development, you can start the Lambda runtime emulator using:
```bash
cargo lambda start
```
This starts a local server that mimics AWS Lambda's environment, allowing you to test your function by sending requests to `http://localhost:9000/2015-03-31/functions/function/invocations`.

## IAM Permissions
Ensure the deployment IAM role has the following permissions:
1. `AWSLambda_FullAccess`
2. `IAMFullAccess`
3. `AWSLambdaBasicExecutionRole`




# Step Functions workflow coordinating Lambdas

In this project, we've established a workflow using AWS Step Functions to coordinate two AWS Lambda functions: `generate_positive_number` and `rust_lambda_sqrt`. This orchestrated process involves generating a random positive number and calculating its square root in a seamless, automated sequence.

### Workflow Overview

1. **Generate Random Number**: The first Lambda function, `generate_positive_number`, generates a random positive number less than 1000. This function doesn't require any input to produce an output.

![Generate positive number](Images/Lambda_generate_positive_number.png)

2. **Calculate Square Root**: The second Lambda function, `rust_lambda_sqrt`, takes the output from the `generate_positive_number` function and calculates the square root of the provided number.

![Rust Lambda sqrt](Images/Lambda_rust_lambda_sqrt.png)

### State Machine Definition

The state machine that manages this workflow is defined as follows:

```json
{
  "Comment": "A workflow that generates a random number and calculates its square root.",
  "StartAt": "GenerateNumber",
  "States": {
    "GenerateNumber": {
      "Type": "Task",
      "Resource": "arn:aws:lambda:us-east-2:730335662926:function:generate_positive_number",
      "Next": "CalculateSquareRoot"
    },
    "CalculateSquareRoot": {
      "Type": "Task",
      "Resource": "arn:aws:lambda:us-east-2:730335662926:function:rust_lambda_sqrt",
      "End": true
    }
  }
}
```

We can also check via the AWS step function as:
![step function execution](Images/Step_Function_Execution.png)
![step function flow](Images/Step_Function_Flow.png)

### Execution Flow
1. The process starts with the GenerateNumber state, invoking the `generate_positive_number` Lambda function.
2. Upon successful execution, the output (value) is passed automatically to the CalculateSquareRoot state.
3. The `rust_lambda_sqrt` function then processes this number to compute its square root, marking the end of the workflow upon completion.

This state machine ensures that data flows correctly between the two Lambda functions and manages the execution order and state transitions, providing a robust solution for automated data processing tasks.


# Orchestrate data processing pipeline

In this project, we implemented an orchestrated data processing pipeline using AWS services. The pipeline uses AWS Lambda functions and AWS Step Functions to process data efficiently and reliably.

### Pipeline Components

1. **Lambda Functions**:
   - `generate_positive_number`: Generates a random positive number less than 1000.
   - `rust_lambda_sqrt`: Receives the output from `generate_positive_number` and calculates the square root.

2. **AWS Step Functions**:
   - Coordinates the Lambda functions, ensuring that data flows seamlessly from generation to processing.

### Workflow Automation with Step Functions

The Step Functions state machine is designed to:
- Start with the `generate_positive_number` function.
- Pass its output to the `rust_lambda_sqrt` function.
- Each function is triggered automatically, with outputs and inputs managed by Step Functions.

```json
{
  "Comment": "Workflow to generate a number and calculate its square root.",
  "StartAt": "GenerateNumber",
  "States": {
    "GenerateNumber": {
      "Type": "Task",
      "Resource": "arn:aws:lambda:your-region:your-account-id:function:generate_positive_number",
      "Next": "CalculateSquareRoot"
    },
    "CalculateSquareRoot": {
      "Type": "Task",
      "Resource": "arn:aws:lambda:your-region:your-account-id:function:rust_lambda_sqrt",
      "End": true
    }
  }
}
```

# CI/CD Integration Using GitLab
This CI/CD setup not only automates the deployment process but also ensures that the code is tested and built in a clean, controlled environment, reducing the chances of deployment errors and downtime.
```
stages:
  - generate_positive_number
  - rust_lambda_sqrt

generate_positive_number:
  stage: generate_positive_number
  image: rust:latest
  script:
    - rustup default stable
    - apt-get update && apt-get install -y wget unzip xz-utils
    - wget https://ziglang.org/download/0.9.1/zig-linux-x86_64-0.9.1.tar.xz
    - tar -xf zig-linux-x86_64-0.9.1.tar.xz -C /usr/local
    - export PATH=$PATH:/usr/local/zig-linux-x86_64-0.9.1
    - cargo install cargo-lambda
    - apt-get install -y zip
    - apt-get install -y musl-tools
    - rustup target add x86_64-unknown-linux-musl
    - cd calculate_frequency
    - export AWS_ACCESS_KEY_ID=$AWS_ACCESS_KEY_ID
    - export AWS_SECRET_ACCESS_KEY=$AWS_SECRET_ACCESS_KEY
    - export AWS_DEFAULT_REGION=$AWS_DEFAULT_REGION
    - cargo lambda build --release
    - cargo lambda deploy
  only:
    - main

rust_lambda_sqrt:
  stage: rust_lambda_sqrt
  image: rust:latest
  script:
    - rustup default stable
    - apt-get update && apt-get install -y wget unzip xz-utils
    - wget https://ziglang.org/download/0.9.1/zig-linux-x86_64-0.9.1.tar.xz
    - tar -xf zig-linux-x86_64-0.9.1.tar.xz -C /usr/local
    - export PATH=$PATH:/usr/local/zig-linux-x86_64-0.9.1
    - cargo install cargo-lambda
    - apt-get install -y zip
    - apt-get install -y musl-tools
    - rustup target add x86_64-unknown-linux-musl
    - cd filter_high_frequency
    - export AWS_ACCESS_KEY_ID=$AWS_ACCESS_KEY_ID
    - export AWS_SECRET_ACCESS_KEY=$AWS_SECRET_ACCESS_KEY
    - export AWS_DEFAULT_REGION=$AWS_DEFAULT_REGION
    - cargo lambda build --release
    - cargo lambda deploy
  only:
    - main
```


