use lambda_runtime::{handler_fn, Context, Error};
use serde::{Deserialize, Serialize};
use serde_json::Value;

#[derive(Deserialize)]
struct Request {
    value: f64,
}

#[derive(Serialize)]
struct Response {
    result: f64,
}

#[tokio::main]
async fn main() -> Result<(), Error> {
    lambda_runtime::run(handler_fn(calculate_sqrt)).await?;
    Ok(())
}

async fn calculate_sqrt(event: Value, _: Context) -> Result<Value, Error> {
    let request: Request = serde_json::from_value(event)?;
    let result = request.value.sqrt();  // Calculate the square root

    let response = Response {
        result,
    };

    Ok(serde_json::to_value(response)?)
}

