use lambda_runtime::{handler_fn, Context, Error};
use serde_json::{json, Value};
use rand::Rng;

#[tokio::main]
async fn main() -> Result<(), Error> {
    lambda_runtime::run(handler_fn(generate_number)).await?;
    Ok(())
}

async fn generate_number(_event: Value, _: Context) -> Result<Value, Error> {
    let mut rng = rand::thread_rng();
    let number: u32 = rng.gen_range(1..1000);
    Ok(json!({ "value": number }))
}
